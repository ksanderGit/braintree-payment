package com.sfox.stripepayment.service;

import com.google.gson.Gson;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.PaymentIntent;
import com.stripe.param.PaymentIntentCreateParams;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


@Service
public class StripeService {

    @Value("${stripe.key.secret}")
    private String API_SECRET_KEY;

    @Value("${stripe.key.public}")
    private String API_PUBLIC_KEY;

    private static Gson gson = new Gson();

    public String createIntent() throws StripeException {
        Stripe.apiKey = API_SECRET_KEY;
        PaymentIntentCreateParams params =
                PaymentIntentCreateParams
                        .builder()
                        .setAmount((new Long(1L * 100)))
                        .setCurrency("usd")
                        .setAutomaticPaymentMethods(createPaymentIntentParams())
                        .build();

        // Create a PaymentIntent with the order amount and currency
        PaymentIntent paymentIntent = PaymentIntent.create(params);

        return gson.toJson(paymentIntent.getClientSecret());
    }

    public String getToken() {
        return API_PUBLIC_KEY;
    }

    private PaymentIntentCreateParams.AutomaticPaymentMethods createPaymentIntentParams() {
        return PaymentIntentCreateParams
                .AutomaticPaymentMethods
                .builder()
                .setEnabled(true)
                .build();
    }
}
