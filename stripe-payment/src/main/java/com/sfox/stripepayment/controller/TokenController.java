package com.sfox.stripepayment.controller;

import com.sfox.stripepayment.service.StripeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TokenController {
    @Autowired
    private StripeService stripeService;

    @CrossOrigin
    @GetMapping("/token")
    public String getToken() {
        return stripeService.getToken();
    }
}
