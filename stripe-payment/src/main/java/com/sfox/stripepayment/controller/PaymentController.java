package com.sfox.stripepayment.controller;


import com.sfox.stripepayment.service.StripeService;
import com.stripe.exception.StripeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentController {

    @Autowired
    private StripeService stripeService;

    @CrossOrigin
    @PostMapping("/create-payment-intent")
    public String createIntent() throws StripeException {
        return stripeService.createIntent();
    }
}

