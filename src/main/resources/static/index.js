class BraintreeClient {
    form = document.getElementById('payment-form');
    applePayButton = document.getElementById('ckoApplePay');
    paymentsClient = new google.payments.api.PaymentsClient({environment: 'TEST'});
    ApplePaySession = window.ApplePaySession;
    authorizationToken = '';
    clientInstance;
    deviceData;

    constructor() {
    }

    async getAuthorizationToken() {
        const options = {
            method: 'GET',
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            }
        };

        const token = fetch('http://localhost:8080/token', options)
            .then(response => response.text())
            .catch(error => console.log('Send error: ', error))
        this.authorizationToken = await token;

        await this.createBraintreeClient();
    }

    checkAuthorizationToken(code) {
        if(code === 'CLIENT_INVALID_AUTHORIZATION') {

        }
    }

    createBraintreeClient() {
        braintree.client.create({
            authorization: this.authorizationToken
        })
            .then((clientInstance) => {
                this.clientInstance = clientInstance;
                this.getDeviceData();
            })
            .catch((error) => {
                console.log('Error: ', error);
                this.checkAuthorizationToken(error.code);
            });
    }

    getDeviceData() {
        braintree.dataCollector.create({
            client: this.clientInstance
        }).then((dataCollectorInstance) => {
            this.deviceData = dataCollectorInstance.deviceData;
            this.paypalCheckoutCreate();
            this.applePayCheckoutCreate();
            this.createCardForm();
            this.googlePayCheckoutCreate();
        }).catch((error) => {
            console.log('Error: ', error);
        });
    }

    googlePayCheckoutCreate() {
        braintree.googlePayment.create({
            client: this.clientInstance,
            googlePayVersion: 2,
            googleMerchantId: '01234567890123456789' // Optional in sandbox; if set in sandbox, this value must be a valid production Google Merchant ID
        }).then((googlePaymentInstance) => {
            this.googlePayButtonCreateAndClick(googlePaymentInstance);
        });
    }

    applePayCheckoutCreate() {
        if (window.ApplePaySession) {
            if (this.ApplePaySession.canMakePayments()) {
                braintree.applePay.create({
                    client: this.clientInstance
                }).then((applePayInstance) => {
                    this.applePayButtonClick(applePayInstance);
                });
            }
        }
    }

    googlePayButtonCreateAndClick(googlePaymentInstance) {
        const button = this.paymentsClient.createButton();
        document.getElementById('container').appendChild(button);
        this.paymentsClient.isReadyToPay({
            apiVersion: 2,
            apiVersionMinor: 0,
            allowedPaymentMethods: googlePaymentInstance.createPaymentDataRequest().allowedPaymentMethods,
            existingPaymentMethodRequired: true
        }).then((response) => {
            if (response.result) {
                button.addEventListener('click', (event) => {
                    event.preventDefault();

                    const paymentDataRequest = googlePaymentInstance.createPaymentDataRequest({
                        transactionInfo: {
                            currencyCode: 'USD',
                            totalPriceStatus: 'FINAL',
                            totalPrice: '100.00'
                        }
                    });

                    const cardPaymentMethod = paymentDataRequest.allowedPaymentMethods[0];
                    cardPaymentMethod.parameters.billingAddressRequired = true;
                    cardPaymentMethod.parameters.billingAddressParameters = {
                        format: 'FULL',
                        phoneNumberRequired: true
                    };

                    this.paymentsClient.loadPaymentData(paymentDataRequest).then(function(paymentData) {
                        return googlePaymentInstance.parseResponse(paymentData);
                    }).then((result) => {
                        this.sendPayment({
                            deviceData: this.deviceData,
                            currencyCode: result.nonce,
                            amount: 10
                        })
                    }).catch((error) => {
                        console.log('Error: ', error);
                    });
                });
            }
        });
    }

    applePayButtonClick(applePayInstance) {
        this.applePayButton.addEventListener('click', (event) => {
            event.preventDefault();
            const paymentRequest = applePayInstance.createPaymentRequest({
                countryCode: 'US',
                currencyCode: 'USD',
                supportedNetworks: ['visa', 'masterCard', 'amex', 'discover'],
                merchantCapabilities: ['supports3DS'],
                total: {label: 'Your Merchant Name', amount: '10.00'},
            });

            const session = new ApplePaySession(3, paymentRequest);

            session.onvalidatemerchant = function (event) {
                applePayInstance.performValidation({
                    validationURL: event.validationURL,
                    displayName: 'My Store'
                }).then((merchantSession) => {
                    session.completeMerchantValidation(merchantSession);
                }).catch((validationErr) => {
                    console.error('Error validating merchant:', validationErr);
                    session.abort();
                });
            };

            session.onpaymentauthorized = function (event) {
                applePayInstance.tokenize({
                    token: event.payment.token
                }).then(function (payload) {

                    session.completePayment(ApplePaySession.STATUS_SUCCESS);
                }).catch(function (tokenizeErr) {
                    session.completePayment(ApplePaySession.STATUS_FAILURE);
                });
            };
            session.begin();
        });
    }

    paypalCheckoutCreate() {
        braintree.paypalCheckout.create({
            client: this.clientInstance
        }).then((paypalCheckoutInstance) => {
            return this.loadSDKPayPal(paypalCheckoutInstance);
        }).then((paypalCheckoutInstance) => {
            this.payPalButton(paypalCheckoutInstance);
        });
    }

    loadSDKPayPal(paypalCheckoutInstance) {
        return paypalCheckoutInstance.loadPayPalSDK({
            currency: 'USD',
            intent: 'capture'
        })
    }

    payPalButton(paypalCheckoutInstance) {
        const that = this;
        return paypal.Buttons({
            fundingSource: paypal.FUNDING.PAYPAL,

            createOrder: function () {
                return paypalCheckoutInstance.createPayment({
                    flow: 'checkout', // Required
                    amount: 10.00, // Required
                    currency: 'USD', // Required, must match the currency passed in with loadPayPalSDK
                    requestBillingAgreement: true, // Required
                    billingAgreementDetails: {
                        description: 'Description of the billng agreement to display to the customer'
                    },

                    intent: 'capture', // Must match the intent passed in with loadPayPalSDK

                    enableShippingAddress: true,
                    shippingAddressEditable: false,
                    shippingAddressOverride: {
                        recipientName: 'Scruff McGruff',
                        line1: '1234 Main St.',
                        line2: 'Unit 1',
                        city: 'Chicago',
                        countryCode: 'US',
                        postalCode: '60652',
                        state: 'IL',
                        phone: '123.456.7890'
                    }
                });
            },

            onApprove: function (data, actions) {
                return paypalCheckoutInstance.tokenizePayment(data).then((payload) =>  {
                    that.sendPayment({
                        deviceData: that.deviceData,
                        currencyCode: payload.nonce,
                        amount: 10
                    });
                });
            },

            onCancel: function (data) {
                console.log('PayPal payment cancelled', JSON.stringify(data, 0, 2));
            },

            onError: function (err) {
                console.error('PayPal error', err);
            }
        }).render('#paypal-button');
    }

    createCardForm() {
        braintree.dropin.create({
            authorization: this.authorizationToken,
            container: document.getElementById('dropin-container'),
        }).then((dropinInstance) => {
            this.submitCardForm(dropinInstance);
        }).catch((error) => {
            console.log('Error: ', error);
        });
    }

    submitCardForm(dropinInstance) {
        const that = this
        this.form.addEventListener('submit', (event) => {
            event.preventDefault();

            dropinInstance.requestPaymentMethod().then((payload) => {
                document.getElementById('nonce').value = payload.nonce;
                const paymentData = {
                    deviceData: this.deviceData,
                    currencyCode: payload.nonce,
                    amount: 10
                }
                that.sendPayment(paymentData).then(() => {
                    this.form.submit();
                });
            }).catch((error) => {
                console.log('Error: ', error);
            });
        });
    }

    sendPayment(data) {
        const options = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
            }
        };

        fetch('http://localhost:8080/payments', options)
            .then(response => response.json())
            .then(response => {
                console.log(response);
            })
            .catch(error => console.log('Send error: ', error));

    }
}

const braintreeClient = new BraintreeClient();
braintreeClient.getAuthorizationToken().then();
