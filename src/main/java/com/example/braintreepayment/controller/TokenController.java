package com.example.braintreepayment.controller;

import com.example.braintreepayment.service.impl.PaymentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TokenController {
    @Autowired
    private PaymentServiceImpl paymentService;

    @CrossOrigin
    @GetMapping("/token")
    public String getToken() {
        return paymentService.generateToken();
    }
}
