package com.example.braintreepayment.controller;

import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.example.braintreepayment.dto.PaymentRequest;
import com.example.braintreepayment.service.impl.PaymentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentController {
    @Autowired
    private PaymentServiceImpl paymentService;

    @GetMapping("/getOrder")
    public PaymentRequest getOrder(@RequestParam String orderId) {
        return paymentService.findOrderById(orderId);
    }

    @CrossOrigin
    @PostMapping("/payments")
    public Result<Transaction> checkout(@RequestBody PaymentRequest paymentRequest) {
        return paymentService.saleTransaction(paymentRequest);
    }


}
