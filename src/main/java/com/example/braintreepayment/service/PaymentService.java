package com.example.braintreepayment.service;

import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.TransactionRequest;
import com.example.braintreepayment.dto.PaymentRequest;

public interface PaymentService {
    String generateToken();
    PaymentRequest findOrderById(String orderId);
    Result<Transaction> saleTransaction(PaymentRequest paymentRequest);
}
