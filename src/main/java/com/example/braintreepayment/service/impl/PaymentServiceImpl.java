package com.example.braintreepayment.service.impl;

import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Environment;
import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.TransactionRequest;
import com.example.braintreepayment.dto.ApplePaymentRequest;
import com.example.braintreepayment.dto.PaymentRequest;
import com.example.braintreepayment.service.PaymentService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;

@Service
public class PaymentServiceImpl implements PaymentService {
    private PaymentRequest paymentRequest;
    private static BraintreeGateway gateway = new BraintreeGateway(
            Environment.SANDBOX,
            "8bgh7p3znh734pnx",
            "zwzgwyfh3ffzjbr9",
            "276c4290ebcb378472641d594f2165b6"
    );

    @Override
    public String generateToken() {
        return gateway.clientToken().generate();
    }

    @Override
    public PaymentRequest findOrderById(String orderId) {
        createPaymentRequest(orderId);
        return paymentRequest;
    }

    @Override
    public Result<Transaction> saleTransaction(PaymentRequest paymentRequest) {
        return gateway.transaction().sale(createTransaction(paymentRequest));
    }

    private TransactionRequest createTransaction(PaymentRequest paymentRequest) {
        return new TransactionRequest()
                .amount(new BigDecimal(paymentRequest.getAmount()))
                .paymentMethodNonce(paymentRequest.getCurrencyCode())
                .deviceData(paymentRequest.getDeviceData())
                .options()
                .submitForSettlement(true)
                .done();
    }

    private void createPaymentRequest(String orderId) {
        if (orderId.equals("applePay")) {
            paymentRequest = new ApplePaymentRequest();
            ApplePaymentRequest applePaymentRequest = (ApplePaymentRequest) paymentRequest;
            applePaymentRequest.setCountryCode("US");
            applePaymentRequest.setCurrencyCode("USD");
            applePaymentRequest.setSupportedNetworks(Set.of("visa", "masterCard", "amex", "discover"));
            applePaymentRequest.setMerchantCapabilities(Set.of("supports3DS"));
            applePaymentRequest.setTotal(Map.of("label", "Your Merchant Name", "amount", "10.00"));
        } else {
            paymentRequest = new PaymentRequest();
            paymentRequest.setAmount("10");
            paymentRequest.setDeviceData("");
            paymentRequest.setCurrencyCode("USD");
        }
    }
}
