package com.example.braintreepayment.dto;

import lombok.Data;

import java.util.Map;
import java.util.Set;

@Data
public class ApplePaymentRequest extends PaymentRequest {
    public String countryCode;
    private Set<String> supportedNetworks;
    private Set<String> merchantCapabilities;
    private Map<String, String> total;
}
