package com.example.braintreepayment.dto;

import lombok.Data;

@Data
public class PaymentRequest {
    private String amount;
    private String currencyCode;
    private String deviceData;
}
